﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Ten kod został wygenerowany przez narzędzie.
//     Wersja wykonawcza:4.0.30319.42000
//
//     Zmiany w tym pliku mogą spowodować nieprawidłowe zachowanie i zostaną utracone, jeśli
//     kod zostanie ponownie wygenerowany.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WCFAirportClient.ServiceReference1 {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IAirportService")]
    public interface IAirportService {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/GetData", ReplyAction="http://tempuri.org/IAirportService/GetDataResponse")]
        string GetData(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/GetData", ReplyAction="http://tempuri.org/IAirportService/GetDataResponse")]
        System.Threading.Tasks.Task<string> GetDataAsync(int value);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getSourceStationNamesList", ReplyAction="http://tempuri.org/IAirportService/getSourceStationNamesListResponse")]
        string[] getSourceStationNamesList();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getSourceStationNamesList", ReplyAction="http://tempuri.org/IAirportService/getSourceStationNamesListResponse")]
        System.Threading.Tasks.Task<string[]> getSourceStationNamesListAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getDestinationStationNamesList", ReplyAction="http://tempuri.org/IAirportService/getDestinationStationNamesListResponse")]
        string[] getDestinationStationNamesList();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getDestinationStationNamesList", ReplyAction="http://tempuri.org/IAirportService/getDestinationStationNamesListResponse")]
        System.Threading.Tasks.Task<string[]> getDestinationStationNamesListAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getTrips", ReplyAction="http://tempuri.org/IAirportService/getTripsResponse")]
        string[] getTrips(string startStation, string destinationStation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getTrips", ReplyAction="http://tempuri.org/IAirportService/getTripsResponse")]
        System.Threading.Tasks.Task<string[]> getTripsAsync(string startStation, string destinationStation);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getTripsWithTime", ReplyAction="http://tempuri.org/IAirportService/getTripsWithTimeResponse")]
        string[] getTripsWithTime(string startStation, string destinationStation, System.DateTime startDate, System.DateTime endDate);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IAirportService/getTripsWithTime", ReplyAction="http://tempuri.org/IAirportService/getTripsWithTimeResponse")]
        System.Threading.Tasks.Task<string[]> getTripsWithTimeAsync(string startStation, string destinationStation, System.DateTime startDate, System.DateTime endDate);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IAirportServiceChannel : WCFAirportClient.ServiceReference1.IAirportService, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class AirportServiceClient : System.ServiceModel.ClientBase<WCFAirportClient.ServiceReference1.IAirportService>, WCFAirportClient.ServiceReference1.IAirportService {
        
        public AirportServiceClient() {
        }
        
        public AirportServiceClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public AirportServiceClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AirportServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public AirportServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public string GetData(int value) {
            return base.Channel.GetData(value);
        }
        
        public System.Threading.Tasks.Task<string> GetDataAsync(int value) {
            return base.Channel.GetDataAsync(value);
        }
        
        public string[] getSourceStationNamesList() {
            return base.Channel.getSourceStationNamesList();
        }
        
        public System.Threading.Tasks.Task<string[]> getSourceStationNamesListAsync() {
            return base.Channel.getSourceStationNamesListAsync();
        }
        
        public string[] getDestinationStationNamesList() {
            return base.Channel.getDestinationStationNamesList();
        }
        
        public System.Threading.Tasks.Task<string[]> getDestinationStationNamesListAsync() {
            return base.Channel.getDestinationStationNamesListAsync();
        }
        
        public string[] getTrips(string startStation, string destinationStation) {
            return base.Channel.getTrips(startStation, destinationStation);
        }
        
        public System.Threading.Tasks.Task<string[]> getTripsAsync(string startStation, string destinationStation) {
            return base.Channel.getTripsAsync(startStation, destinationStation);
        }
        
        public string[] getTripsWithTime(string startStation, string destinationStation, System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.getTripsWithTime(startStation, destinationStation, startDate, endDate);
        }
        
        public System.Threading.Tasks.Task<string[]> getTripsWithTimeAsync(string startStation, string destinationStation, System.DateTime startDate, System.DateTime endDate) {
            return base.Channel.getTripsWithTimeAsync(startStation, destinationStation, startDate, endDate);
        }
    }
}
