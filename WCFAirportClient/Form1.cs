﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WCFAirportClient.ServiceReference1;

namespace WCFAirportClient
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            AirportServiceClient client = new AirportServiceClient();
            comboBox1.Items.AddRange(client.getSourceStationNamesList());
            comboBox2.Items.AddRange(client.getDestinationStationNamesList());
            client.Close();
            
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "MM/dd/yyyy hh:mm";
            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "MM/dd/yyyy hh:mm";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            textBox3.Clear();

            string[] trips = null;
            string selectedVal = "";
            string selectedVal2 = "";
            try
            {
                selectedVal = comboBox1.Text;
                bool exist = false;
                foreach (String item in comboBox1.Items)
                {
                    if (item.Contains(selectedVal)) exist = true;
                }
                if (!exist) throw new NullReferenceException();
            }
            catch(NullReferenceException nre)
            {
                textBox3.Text += " Source station " + selectedVal + " does not exits. ";
            }
            try
            {
                selectedVal2 = comboBox2.Text;
                bool exist = false;
                foreach (String item in comboBox2.Items)
                {
                    if (item.Contains(selectedVal2)) exist = true;
                }
                if (!exist) throw new NullReferenceException();
            }
            catch (NullReferenceException nre)
            {
                textBox3.Text += " Destination station " + selectedVal2 + " does not exits. ";
            }
            AirportServiceClient client = new AirportServiceClient();
            
            if (checkBox1.Checked)
                trips = client.getTripsWithTime(selectedVal, selectedVal2, dateTimePicker1.Value, dateTimePicker2.Value);
            else
                trips = client.getTrips(selectedVal, selectedVal2);
            foreach (string row in trips)
            {
                listBox1.Items.Add(row);
            }
            client.Close();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
