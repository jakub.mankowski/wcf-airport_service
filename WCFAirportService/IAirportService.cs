﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFAirportService
{
    // UWAGA: możesz użyć polecenia „Zmień nazwę” w menu „Refaktoryzuj”, aby zmienić nazwę interfejsu „IAirportService” w kodzie i pliku konfiguracji.
    [ServiceContract]
    public interface IAirportService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        List<string> getSourceStationNamesList();

        [OperationContract]
        List<string> getDestinationStationNamesList();

        [OperationContract]
        List<string> getTrips(string startStation, string destinationStation);

        [OperationContract]
        List<string> getTripsWithTime(string startStation, string destinationStation, DateTime startDate, DateTime endDate);


    }
    [DataContract]
    public class Trip
    {
        [DataMember]
        string startPoint;
        [DataMember]
        string startTime;
        [DataMember]
        string endPoint;
        [DataMember]
        string endTime;

        public Trip(string StationA, string StationB, string TimeFrom, string TimeTo)
        {
            this.startPoint = StationA;
            this.endPoint = StationB;
            this.startTime = TimeFrom;
            this.endTime = TimeTo;
        }
        public string StartPoint
        {
            get
            {
                return startPoint;
            }
            set
            {
                startPoint = value;
            }
        }

        public string StartTime
        {
            get
            {
                return startTime;
            }
            set
            {
                startTime = value;
            }
        }

        public string EndPoint
        {
            get
            {
                return endPoint;
            }
            set
            {
                endPoint = value;
            }
        }

        public string EndTime
        {
            get
            {
                return endTime;
            }
            set
            {
                endTime = value;
            }
        }

    }
    [DataContract]
    public class Trips
    {
        List<Trip> allTrips = new List<Trip>();
        [DataMember]
        public List<Trip> AllTrips
        {
            get { return allTrips; }
            set { allTrips = value; }
        }

    }
}
