﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCFAirportService
{
    public class AirportService : IAirportService
    {
        List<Trip> allTrips = new List<Trip>();
        public string GetData(int value)
        {
            throw new NotImplementedException();
        }
        public List<string> getSourceStationNamesList()
        {
            List<Trip> allTrips = ParseCVS();
            List<string> sourceStations = new List<string>();
            foreach (Trip record in allTrips)
                sourceStations.Add(record.StartPoint);
            return sourceStations.Distinct().ToList();
        }
        public List<string> getDestinationStationNamesList()
        {
            List<Trip> allTrips = ParseCVS();
            List<string> destinationStations = new List<string>();
            foreach (Trip record in allTrips)
                destinationStations.Add(record.EndPoint);
            return destinationStations.Distinct().ToList();
        }

        public List<string> getTrips(string startStation, string destinationStation)
        {
            return getTripsWithTime(startStation, destinationStation, DateTime.MinValue, DateTime.MaxValue);
        }
        public List<string> getTripsWithTime(string startStation, string destinationStation, DateTime startDate, DateTime endDate)
        {
            List<Trip> directList = ParseCVS();
            List<string> indirectList = new List<string>();
            List<string> outputList = new List<string>();
            foreach (Trip record in directList)
            {
                if (record.StartPoint.Equals(startStation) && record.EndPoint.Equals(destinationStation))
                    if (DateTime.Compare(Convert.ToDateTime(record.StartTime), startDate) >= 0 && DateTime.Compare(Convert.ToDateTime(record.EndTime), endDate) <= 0)
                        outputList.Add(toStringFlyData(record));
            }
            indirectList = findIndirectConnections(startStation, destinationStation, startDate, endDate);
            foreach (string i in indirectList)
            {
                outputList.Add(i);
            }

            if (!outputList.Any())
                outputList.Add("Brak połączenia między stacjami");
            return outputList;
        }
        private List<string> findIndirectConnections(string start, string end, DateTime date, DateTime dateEnd)
        {

            List<Trip> startTemp = new List<Trip>();
            List<Trip> temp = new List<Trip>();
            foreach (Trip trip in allTrips)
            {
                if (trip.EndPoint == end)
                {
                    temp.Add(trip);
                }
                else if (trip.StartPoint == start)
                {
                    startTemp.Add(trip);
                }
            }
            List<string> indirectRoutes = new List<string>();

            foreach (Trip startT in startTemp)
            {
                foreach (Trip endT in temp)
                {

                    if (startT.EndPoint == endT.StartPoint && DateTime.Compare(Convert.ToDateTime(startT.StartTime), Convert.ToDateTime(endT.EndTime)) <= 0 && DateTime.Compare(Convert.ToDateTime(endT.EndTime), dateEnd) <= 0)
                    {
                        string res = startT.StartPoint + " " + startT.StartTime + " " + startT.EndPoint + " " + startT.EndTime + " " + endT.StartPoint + " " + endT.StartTime + " " + endT.EndPoint + " " + endT.EndTime;

                        indirectRoutes.Add(res);


                    }


                }
            }
            List<string> indirectConnections = new List<string>();
            int index = 1;
            foreach (String rout in indirectRoutes)
            {
                indirectConnections.Add(rout);
                index++;
            }

            return indirectConnections;
        }
        public List<Trip> ParseCVS()
        {
            string[] csvLines = File.ReadAllLines(@"C:\Users\Jakub\source\repos\WCFAirportService\WCFAirportService\flights.csv");

            foreach (string line in csvLines.Skip(1))
            {
                var splitedLine = line.Split(';');
                allTrips.Add(
                    new Trip(
                        splitedLine[0],
                        splitedLine[1],
                        splitedLine[2],
                        splitedLine[3]
                        ));
            }
            return allTrips;
        }
        public string toStringFlyData(Trip record)
        {
            return record.StartPoint.ToString() + " " + record.StartTime.ToString() + " " +
                            record.EndPoint.ToString() + " " + record.EndTime.ToString();
        }
    }
}
